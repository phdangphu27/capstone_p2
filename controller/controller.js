const nameEl = document.getElementById("name");
const priceEl = document.getElementById("price");
const imgEl = document.getElementById("img");
const descEl = document.getElementById("desc");
const searchEl = document.getElementById("search");

const takeFormInput = function () {
  const name = nameEl.value;
  const price = priceEl.value;
  const image = imgEl.value;
  const desc = descEl.value;

  return {
    name,
    price,
    image,
    desc,
  };
};

const renderProducts = function (phones) {
  let contentHTML = "";

  phones.forEach((phone) => {
    contentHTML += `<tr>
        <td>${phone.id}</td>
        <td>${phone.name}</td>
        <td>${phone.price}</td>
        <td><img src="${phone.image}"/></td>
        <td>${phone.desc}</td>
        <td>
        <button class="btn" onclick="btnDelete(${phone.id})"><i class="fa fa-trash"></i></button>
        <button class="btn" data-toggle="modal"
        data-target="#exampleModal" onclick="btnModify(${phone.id})"><i class="fa fa-edit"></i></button>
        </td>
        </tr>`;
  });
  document.getElementById("productsBody").innerHTML = contentHTML;
};

const showFormInput = function (product) {
  nameEl.value = product.name;
  priceEl.value = product.price;
  imgEl.value = product.image;
  descEl.value = product.desc;
};

const showMessage = function (idErr, message) {
  document.getElementById(idErr).innerHTML = message;
};

const hideMessage = function (idErr) {
  document.getElementById(idErr).innerHTML = "";
};

const resetForm = function () {
  document.querySelector(".modal-body").reset();
  resetErr();
};

const resetErr = function () {
  const errorElList = document.querySelectorAll(".error");
  errorElList.forEach((error) => {
    error.innerHTML = "";
  });
};

const loaderOn = function () {
  document.querySelector(".loader").style.display = "flex";
};

const loaderOff = function () {
  document.querySelector(".loader").style.display = "none";
};

const hideBtnUpdate = function () {
  document.getElementById("btnUpdate").classList.add("d-none");
};

const showBtnUpdate = function () {
  document.getElementById("btnUpdate").classList.remove("d-none");
};

const hideBtnAdd = function () {
  document.getElementById("btnAdd").classList.add("d-none");
};

const showBtnAdd = function () {
  document.getElementById("btnAdd").classList.toggle("d-none");
};

const resetModal = function (id, modal) {
  document.getElementById(id).setAttribute("data-dismiss", modal);
};
